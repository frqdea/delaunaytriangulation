import numpy
import math
import copy
import Tkinter
import time
DEBUG_FLAG = True


class DelaunaySequential:
    EPS = 1.00000000e-14

    def __init__(self, area_boundary_points):
        self.area_boundary_points = area_boundary_points
        self.area_boundary_edges = set()
        self.triangles = []
        self.edge_to_triangles_map = {}
        self.appliedBoundaryEdges = None
        self.holes = None
        self.time_flip_edges = 0.0
        self.time_add_point = 0.0
        self.time_flip_single_edge = 0.0
        self.time_get_area = 0.0
        self.counter = 0

        gravity_center = numpy.zeros((2, ), numpy.float64)
        for point in area_boundary_points:
            gravity_center += point
        gravity_center = gravity_center / len(area_boundary_points)

        def distance_square(pt):
            d = pt - gravity_center
            return numpy.dot(d, d)

        self.area_boundary_points.sort(key=distance_square)

        area = 0.0
        index = 0
        while index + 2 < len(area_boundary_points):
            area = self.get_area(index, index+1, index+2)
            if abs(area) < self.EPS:
                del self.area_boundary_points[index]
            else:
                break
        if index <= len(self.area_boundary_points) - 3:
            triangle = [index, index+1, index+2]
            self.make_counter_clockwise(triangle)
            self.triangles.append(triangle)

            edge01 = (triangle[0], triangle[1])
            self.area_boundary_edges.add(edge01)
            edge12 = (triangle[1], triangle[2])
            self.area_boundary_edges.add(edge12)
            edge20 = (triangle[2], triangle[0])
            self.area_boundary_edges.add(edge20)

            edge01 = self.make_key(edge01[0], edge01[1])
            self.edge_to_triangles_map[edge01] = [0, ]
            edge12 = self.make_key(edge12[0], edge12[1])
            self.edge_to_triangles_map[edge12] = [0, ]
            edge20 = self.make_key(edge20[0], edge20[1])
            self.edge_to_triangles_map[edge20] = [0, ]
        else:
            return

        for i in range(3, len(self.area_boundary_points)):
            self.add_point(i)

    def get_triangles(self):
        return self.triangles

    def get_edges(self):
        return self.edge_to_triangles_map.keys()

    def get_area(self, point1, point2, point3):
        start = time.time()
        distance1 = self.area_boundary_points[point2] - self.area_boundary_points[point1]
        distance2 = self.area_boundary_points[point3] - self.area_boundary_points[point1]
        result = distance1[0]*distance2[1] - distance1[1]*distance2[0]
        end = time.time()
        self.time_get_area += (end-start)
        return result

    def is_edge_visible(self, point, edge):
        area = self.get_area(point, edge[0], edge[1])
        return area < self.EPS

    def make_counter_clockwise(self, points):
        area = self.get_area(points[0], points[1], points[2])
        if area < -self.EPS:
            point1, point2 = points[1], points[2]
            points[1], points[2] = point2, point1

    def flip_one_edge(self, edge):
        start = time.time()
        result = set()
        triangles = self.edge_to_triangles_map.get(edge, [])
        if len(triangles) < 2:
            return result

        i_tri1, i_tri2 = triangles
        triangle1 = self.triangles[i_tri1]
        triangle2 = self.triangles[i_tri2]

        opposite_vertex1 = -1
        opposite_vertex2 = -1
        for i in range(3):
            if not triangle1[i] in edge:
                opposite_vertex1 = triangle1[i]
            if not triangle2[i] in edge:
                opposite_vertex2 = triangle2[i]

        da1 = self.area_boundary_points[edge[0]] - self.area_boundary_points[opposite_vertex1]
        db1 = self.area_boundary_points[edge[1]] - self.area_boundary_points[opposite_vertex1]
        da2 = self.area_boundary_points[edge[0]] - self.area_boundary_points[opposite_vertex2]
        db2 = self.area_boundary_points[edge[1]] - self.area_boundary_points[opposite_vertex2]
        cross_prod1 = self.get_area(opposite_vertex1, edge[0], edge[1])
        cross_prod2 = self.get_area(opposite_vertex2, edge[1], edge[0])
        self.counter += 1
        if DEBUG_FLAG:
            time.sleep(0.005)
            time.sleep(0.005)
        dot_prod1 = numpy.dot(da1, db1)
        dot_prod2 = numpy.dot(da2, db2)
        angle1 = abs(math.atan2(cross_prod1, dot_prod1))
        angle2 = abs(math.atan2(cross_prod2, dot_prod2))

        if angle1 + angle2 > math.pi * (1.0 + self.EPS):
            new_triangle1 = [opposite_vertex1, edge[0], opposite_vertex2]
            new_triangle2 = [opposite_vertex1, opposite_vertex2, edge[1]]
            self.triangles[i_tri1] = new_triangle1
            self.triangles[i_tri2] = new_triangle2
            del self.edge_to_triangles_map[edge]

            e = self.make_key(opposite_vertex1, opposite_vertex2)
            self.edge_to_triangles_map[e] = [i_tri1, i_tri2]
            e = self.make_key(opposite_vertex1, edge[1])
            v = self.edge_to_triangles_map[e]
            for i in range(len(v)):
                if v[i] == i_tri1:
                    v[i] = i_tri2
            result.add(e)
            e = self.make_key(opposite_vertex2, edge[0])
            v = self.edge_to_triangles_map[e]
            for i in range(len(v)):
                if v[i] == i_tri2:
                    v[i] = i_tri1
            result.add(e)
            result.add(self.make_key(opposite_vertex1, edge[0]))
            result.add(self.make_key(opposite_vertex2, edge[1]))
        end = time.time()
        self.time_flip_single_edge += (end-start)
        return result

    def flip_edges(self):
        start = time.time()
        edge_set = set(self.edge_to_triangles_map.keys())

        continue_flipping = True
        while continue_flipping:
            new_edge_set = set()
            for edge in edge_set:
                new_edge_set |= self.flip_one_edge(edge)
            edge_set = copy.copy(new_edge_set)
            continue_flipping = (len(edge_set) > 0)
        end = time.time()
        self.time_flip_edges += (end-start)

    def add_point(self, point_index):
        start = time.time()
        boundary_edges_to_remove = set()
        boundary_edges_to_add = set()

        for edge in self.area_boundary_edges:
            if self.is_edge_visible(point_index, edge):
                new_triangle = [edge[0], edge[1], point_index]
                new_triangle.sort()
                self.make_counter_clockwise(new_triangle)
                self.triangles.append(new_triangle)

                e = list(edge[:])
                e.sort()
                i_tri = len(self.triangles) - 1
                self.edge_to_triangles_map[tuple(e)].append(i_tri)

                e1 = [point_index, edge[0]]
                e1.sort()
                e1 = tuple(e1)
                e2 = [edge[1], point_index]
                e2.sort()
                e2 = tuple(e2)
                v1 = self.edge_to_triangles_map.get(e1, [])
                v1.append(i_tri)
                v2 = self.edge_to_triangles_map.get(e2, [])
                v2.append(i_tri)
                self.edge_to_triangles_map[e1] = v1
                self.edge_to_triangles_map[e2] = v2

                boundary_edges_to_remove.add(edge)
                boundary_edges_to_add.add((edge[0], point_index))
                boundary_edges_to_add.add((point_index, edge[1]))
        for boundary_edge in boundary_edges_to_remove:
            self.area_boundary_edges.remove(boundary_edge)
        for boundary_edge in boundary_edges_to_add:
            boundary_edge_sorted = list(boundary_edge)
            boundary_edge_sorted.sort()
            boundary_edge_sorted = tuple(boundary_edge_sorted)
            if len(self.edge_to_triangles_map[boundary_edge_sorted]) == 1:
                self.area_boundary_edges.add(boundary_edge)

        flipped = True
        while flipped:
            flipped = self.flip_edges()
        end = time.time()
        self.time_add_point += (end-start)

    def make_key(self, value1, value2):
        if value1 < value2:
            return value1, value2
        return value2, value1

    def show(self, width=1800, height=900, show_vertices=False, show_contour=[]):
        xmin = min([point[0] for point in self.area_boundary_points])
        ymin = min([point[1] for point in self.area_boundary_points])
        xmax = max([point[0] for point in self.area_boundary_points])
        ymax = max([point[1] for point in self.area_boundary_points])
        padding = 5
        w = width - 2*padding
        h = height - 2*padding
        master = Tkinter.Tk()
        canvas = Tkinter.Canvas(master, width=width, height=height)
        canvas.pack()
        for edge in self.edge_to_triangles_map:
            point1_index, point2_index = edge
            point1_x = padding + int(w*(self.area_boundary_points[point1_index][0] - xmin)/(xmax - xmin))
            point1_y = padding + int(h*(ymax - self.area_boundary_points[point1_index][1])/(ymax - ymin))
            point2_x = padding + int(w*(self.area_boundary_points[point2_index][0] - xmin)/(xmax - xmin))
            point2_y = padding + int(h*(ymax - self.area_boundary_points[point2_index][1])/(ymax - ymin))
            canvas.create_line(point1_x, point1_y, point2_x, point2_y)

        if show_vertices:
            for i in range(len(self.area_boundary_points)):
                point_x = padding + int(w*(self.area_boundary_points[i][0] - xmin)/(xmax - xmin))
                point_y = padding + int(h*(ymax - self.area_boundary_points[i][1])/(ymax - ymin))
                canvas.create_text(point_x, point_y, text=str(i))

        if len(show_contour) > 0:
            for i in range(len(show_contour) - 1):
                point1_x = padding + int(w * (show_contour[i][0] - xmin) / (xmax - xmin))
                point1_y = padding + int(h * (ymax - show_contour[i][1]) / (ymax - ymin))
                point2_x = padding + int(w * (show_contour[i + 1][0] - xmin) / (xmax - xmin))
                point2_y = padding + int(h * (ymax - show_contour[i + 1][1]) / (ymax - ymin))
                canvas.create_line(point1_x, point1_y, point2_x, point2_y, fill='red')
        print self.counter
        Tkinter.mainloop()

    def get_time_elapsed(self):
        return "Time elapsed per method:" +\
                "\nFlip Edges: " + str(self.time_flip_edges) +\
                "\nFlip Single Edge: " + str(self.time_flip_single_edge) +\
                "\nAdd point: " + str(self.time_add_point) +\
                "\nGet Area: " + str(self.time_get_area)
