import random
import numpy
import time
from DelaunayParallel import DelaunayParallel
from DelaunaySequential import DelaunaySequential

parallel = True
if parallel:
    random.seed(56734)
    xyPoints = [numpy.array([random.random(), random.random()]) for i in range(100)]
    start = time.time()
    delaunay_parallel = DelaunayParallel(xyPoints)
    end = time.time()
    print("Total time: " + str(end-start))
    print(delaunay_parallel.get_time_elapsed())
    delaunay_parallel.show()
else:
    random.seed(56734)
    xyPoints = [numpy.array([random.random(), random.random()]) for i in range(100)]
    start = time.time()
    delaunay_sequential = DelaunaySequential(xyPoints)
    end = time.time()
    print("Total time: " + str(end - start))
    print(delaunay_sequential.get_time_elapsed())
    delaunay_sequential.show()
